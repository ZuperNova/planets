const crypto = require('crypto');

class Auth {

    constructor(pool) {
        this.pool = pool;
    }

    async generateKey(req, res) {

        try {
            const apiKey = await crypto.randomBytes(32).toString('hex');
            const result = await this.pool.query('INSERT INTO api_key (key) VALUES ($1) RETURNING id', [apiKey]);

            res.json({
                apiKey
            });

        } catch (error) {
            res.json({
                error: error.message
            });
        }
    }

    async authenticateKey(req) {
        try {
            const { authorization } = req.headers;

            if (!authorization) {
                return {
                    error: 'No API key found.'
                };
            }

            const key = authorization.split(' ')[1];
            const keyResult = await this.pool.query('SELECT * FROM api_key WHERE key = $1 AND active = 1', [key]);

            if (keyResult.rowCount > 0) {
                return true;
            } else {
                return {
                    error: 'Invalid api key'
                };
            }

        } catch (error) {
            return {
                error: error.message
            }
        }
    }
}

module.exports.Auth = Auth;