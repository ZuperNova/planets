const { Pool } = require('pg');

class Db {
    constructor() {
        this.config = {
            user: 'postgres',
            host: 'localhost',
            database: 'universe',
            password: '123456',
            port: 5432,
        };
        this.pool = new Pool(this.config);
    }

    getPool() {
        return this.pool;
    }

    getPlanets(req, res) {
        this.pool.query('SELECT * FROM planets', (err, result) => {
            if(err) {
                res.status(500).json({
                    success: false,
                    message: err
                });
            }
            res.status(200).json(result.rows);
        });
    }

    addPlanet(req, res) {
        const { id, name, size, date_discovered } = req.body;
        this.pool.query(`SELECT name FROM planets WHERE name='${name}'`, (err, result) => {
            if(err) {
                res.status(500).json({
                    success: false,
                    message: err
                });
            } else if(result.rows[0]) {
                res.status(403).json({
                    success: false,
                    message: "The planet already exists!",
                    result: result.rows[0]
                });
            } else {
                this.pool.query(`INSERT INTO planets (id, name, size, date_discovered) VALUES (${id}, '${name}', ${size}, '${date_discovered}') RETURNING *`, (err, result) => {
                    if(err) {
                        res.status(500).json({
                            success: false,
                            message: err
                        });
                    }
                    res.status(200).json(result.rows[0]);
                });
            }
        });
    }

    deletePlanet(req, res) {
        const { id } = req.query;
        this.pool.query(`DELETE FROM planets WHERE id=${id} RETURNING *`, (err, result) => {
            if(err) {
                res.status(500).json({
                    success: false,
                    message: err
                });
            }
            res.status(200).json(result.rows[0]);
        });
    }

    updatePlanet(req, res) {

        const { id, name, size, date_discovered } = req.body;

        this.pool.query(`UPDATE planets SET id=${id}, name='${name}', size=${size}, date_discovered='${date_discovered}' WHERE id=${id} RETURNING *`, (err, result) => {
            if(err) {
                res.status(500).json({
                    success: false,
                    message: err
                });
            }
            res.status(200).json(result.rows[0]);
        });
    }
}

module.exports.db = new Db();