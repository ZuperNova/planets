const express = require('express');
const app = express();
const PORT = 8080;
const { db } = require('./db');
const { Auth } = require('./auth');
const publicPath = [
    '/api/v1/auth/generate-key'
];
const auth = new Auth(db.getPool());

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

app.use(async (req, res, next) => {
    if (publicPath.includes(req.originalUrl)) {
        return next();
    }
    const authResult = await auth.authenticateKey(req, res);

    if (authResult === true) {
        next();
    } else {
        res.status(401).json(authResult);
    }
});



app.get('/api/v1/planets', (req, res) => {
    db.getPlanets(req, res);
});

app.post('/api/v1/planets/add', (req, res) => {
    db.addPlanet(req, res);
});

app.delete('/api/v1/planets/delete', (req, res) => {
    db.deletePlanet(req, res);
});

app.put('/api/v1/planets/update', (req, res) => {
    db.updatePlanet(req, res);
});

app.post('/api/v1/auth/generate-key', (req, res) => auth.generateKey(req, res));

app.listen(PORT, () => {
    console.log(`Started express server on port ${PORT}`);
});